
var flipedImg = ""
var imgId = ""
var count = 0
var found = 0

var images = $(".cards").children()
var cards = []

var init_time = "00:30"
var gametime = init_time.split(":")


function initApp() {
	console.log("App started")

	$("#image_button_start").on('click', start)


	if ( $("div.cards").length ) {
		mixup()

		$(".start-button").on('click', function(){

			$(".cards").removeClass('hid')

			$(this).hide()
			
			$(".cards .holder .boxed_card").on('click', showImage) // adding handler 
			startTimer() // started time
		})
	}


}

function start() {
	var player_name = $("input[name=player]").val()
	console.log(player_name.length)

	if (player_name != "") {
		if (player_name.length > 3) {
			$.post('/set/player/', {"player_name": player_name}, function(data){
				if (data.error == 100) {
					$("#error_message").text(data.message)
				} else {
					window.location.href = '/game/'
				}
			})
		} else {
			$("#error_message").text("Try typing a name a little bit long")
		}
	} else {
		$("#error_message").text("We need you give us your name please")
	}


	$("input[name=player]").on('focus', function(){
		$("#error_message").text("")
	})

}



function mixup() {
	
	var array_image_temp = []
	var array_html_cards = []
	var array_pairs = []


	for (i=0; i<images.length; i++) {
		cardId = $(".holder:nth-child("+(i+1)+") .boxed_card .box").data("id")

		array_image_temp[i] = $(".holder:nth-child("+(i+1)+") .boxed_card .box").data("id")
		array_html_cards[cardId] = $(".holder:nth-child("+(i+1)+") .boxed_card").html() // we need sort holder into a new array
	}


	for (i = 0; i < array_image_temp.length/2; i++) {

		var random_card = _.random(0, array_image_temp.length-1)
		
		var counts = _.countBy(array_pairs)[array_image_temp[random_card]]
		
		if ( counts < 2 || counts == null ) {
			array_pairs.push(array_image_temp[random_card])
			array_pairs.push(array_image_temp[random_card])
		}
		
		

	}


	var pairs_count = []
	var unique_cards = _.uniq(array_pairs)
	for (i=0; i<unique_cards.length; i++) {
		pairs_count[i] = _.countBy(array_pairs)[unique_cards[i]]
	}



	for(i=0; i<array_pairs.length; i++) {
		cards[i] = array_html_cards[ array_pairs[i] ]
	}

	cards = _.shuffle(cards) // we'll shuffle the holder to avoid getting together each other

	
	for (i=0; i<cards.length; i++) {

		$(".holder:nth-child("+(i+1)+")"+" .boxed_card").html(cards[i])

		boxed_card = $(".holder:nth-child("+(i+1)+")"+" .boxed_card")
		idCard = boxed_card.find("div.box").data("id")

		$(".holder:nth-child("+(i+1)+")"+" .boxed_card").attr('id', 'card_'+idCard)
	}

	$(".holder:nth-child(n+"+(cards.length+1)+")").remove()

	
	

}

function startTimer() {

	// check seconds
	if (gametime[1] <= 0) {

		if (gametime[0] <= 10) {
			gametime[0] = "0" + (parseInt(gametime[0]) - 1).toString() // decrease minutes	
		} else {
			gametime[0] = (parseInt(gametime[0]) - 1).toString() // decrease minutes
		}
		

		// decrease seconds 
		ds = 0 // reset seconds to 59

	} else {
		ds = 1 // only decrease
	}


	if (ds == 1) { // decrease seconds
		if (gametime[1] <= 10) {
			gametime[1] = "0" + (parseInt(gametime[1]) - 1)
		} else {
			gametime[1] = (parseInt(gametime[1]) - 1).toString()
		}
	} else {
		gametime[1] = "59"
	}

	// check minutes
	if (gametime[0] > 0) {
		if (gametime[0] <= 10) {
			gametime[0] = "0" + (parseInt(gametime[0]) - 1)
		} else {
			gametime[0] = (parseInt(gametime[0]) - 1).toString()
		}
	}


	//console.log(gametime)
	
	
	$(".gametime").html(gametime[0] + ":" + gametime[1]);
	if (found<Math.floor(cards.length/2)) {
		if (gametime[0] == "00" && gametime[1] == "00") {
			st = 1
		} else {
			st = 0
			setTimeout('startTimer()', 1000);	
		}

	} else {
		st = 2

	}


	if (st == 1) {
		$('.cards').addClass('hid') // hide cards because the player has lost
		stopTimer(st, count, gametime, "You've lost, try again :(")
	} else if (st == 2) {
		stopTimer(st, count, gametime, "You've finished the game in "+count+" movements<br /> on "+(gametime[0]=='00' ? "" : gametime[0] + " minutes with ") + gametime[1]+" seconds")
	}
	

}


function stopTimer(st, c, gt, m) {
	if (st == 2) {
		// sending stats from player
		submitScore(count, gametime)	
	}
	alertify.alert(m)
}



function showImage() {
	
	id = $(this).attr("id")
	//console.log( $("#"+id+" img").css('visibility') )
	//console.log('id: '+id)

	
	if ( $(this).find(".box").hasClass('hid') ) { // it's hidden

		$(this).find('.box').removeClass('hid')
		

		if (flipedImg == "") {
			imgId = id
			flipedImg = $("#"+id+" .box").data("id") 
			//console.log('flipped  '+flipedImg+'     '+imgId+'     '+id)
		} else {
			current = $("#"+id+" .box").data("id")
			//console.log('current  '+current+'      flipedImg  '+flipedImg+'      '+id)
			if (flipedImg != current) { // flipedImg saves a number corresponding to the id card, same as current var
				setTimeout(function(){
					//console.log('id:   '+id+'       imgId: '+imgId)

					$("#"+id+" .box").addClass('hid')
					$("#"+imgId+" .box").addClass('hid')

					imgId = ""
					flipedImg = ""

				}, 400)
			} else {
				$("#"+id+" .box").addClass("opacity")
				$("#"+imgId+" .box").addClass("opacity")

				$("#"+id).unbind('click')
				$("#"+imgId).unbind('click')

				found++ // this one increases a counter of pairs found in game
				flipedImg = ""
				imgId = ""

			}
			
		}
		count++ // counting tries

	}

	//console.log(found+'     '+count)

	
	if (found == Math.floor(cards.length/2)) {
		$(".messageBox").html("<p>You've finished the game in "+count+" movements</p>")
		//console.log("win!!")
	} 
	


}


function submitScore(c, gt) {
	var player = $("#playername").text()
	console.log('submitting   ' + c + '   ' + gt + '   ' + player)

	$.post('/highscore/submit', { playerName: player, time: gt, movPlayer: c })

}


