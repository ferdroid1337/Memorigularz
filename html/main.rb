# encoding: UTF-8
# main.rb

get '/' do
	#home = Home.new

	if session['player_name'].nil?
		erb :index
	else
		redirect('/game/')
	end
    
end


get '/game/' do
	home = Home.new
	
	if session['player_name'].nil?
		redirect('/')
	else
		@array_cards = home.set_cards
		
		erb :game, :locals => { :player_name => session['player_name'] }
	end

end


post '/set/player/' do
	content_type :json

	if (params['player_name'].strip.nil? || params['player_name'].strip.length < 1)
		{'error' => 100, 'message' => 'Input invalid'}.to_json
	else
		session['player_name'] = params['player_name'].strip 
		{'error' => 0}.to_json
	end

end



get '/configure/models' do

	#Task.auto_migrate!
	#Task.create(name: "Jajajj este framework me parece bien rlz ")

end


get '/game/highscores' do
	home = Home.new
	@scores = home.get_highscore
	#puts @scores.inspect

	if session['player_name'].nil?
		redirect('/')
	else 
		erb :highscores, :locals => { :player_name => session['player_name'] }
	end

end


post '/highscore/submit' do
	home = Home.new
	home.append_to_highscore(params['playerName'], params['time'], params['movPlayer'])
end



get '/game/quit' do
	session.clear
	redirect('/')
end


get '/game/about' do

	if session['player_name'].nil?
		redirect('/')
	else
		erb :about, :locals => { :player_name => session['player_name'] }
	end
end
