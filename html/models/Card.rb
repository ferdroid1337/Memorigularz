class Card
	include DataMapper::Resource

	property :id,				Serial
	property :verb,				String, :length => 50, :required => true
	property :pp,				String, :length => 50, :required => true
	property :past,				String, :length => 50, :required => true
	property :type,				Integer, :required => true # regular or irregular
	property :gerund,			Text, :required => true
	property :image,			Text, :default => 'image_default.png'


end
DataMapper.finalize